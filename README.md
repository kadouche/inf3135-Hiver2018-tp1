# Travail pratique 1 : `pandemi.c`

Il s'agit de l'énoncé du travail pratique 1 du cours INF3135 Construction et
maintenance de logiciels, enseigné par Rachid Kadouche. 
Ce document est basé sur le TP1 rédigé par Alexandre Blondin Massé et Alexandre
Terrasa qui ont enseigné le cours INF3135 Construction et maintenance de logiciels
à l'automne 2017, à l'Université du Québec à Montréal.

Le travail doit être fait **seul** et remis au plus tard le **18 février à 23h59**. À partir de
minuit, une pénalité de **2%** par heure de retard sera appliquée.

## Mise en situation

La planète Terre est menacée par un nouveau genre de virus, le virus X. Ce
virus se propage rapidement et les autorités sanitaires mondiales s'inquiètent
des risques de pandémie. Afin de visualiser sa propagation, il vous est demandé
d'écrire un simulateur capable de montrer l'évolution du virus, jour après jour,
au sein d'une population en fonction d'un scénario de départ.

## Objectif

Vous devez concevoir et implémenter le simulateur appelé `pandemic` afin de
sauver la vie humaine.  Le simulateur prend comme argument le nombre de jours à
simuler.  La carte de configuration de la simulation est lue depuis l'entrée
standard (`stdin`).

## Carte de configuration

Une carte de configuration est une matrice de 40 colonnes par 20 lignes
représentant l'état d'infection de la population par le virus X. Chaque cellule
de la carte représente une zone géographique pouvant avoir 3 états:

* Le caractère `H` indique que la zone géographique contient une population
  saine (non infectée par le virus X).

* Le caractère `Q` indique que la zone géographique est en quarantaine, 
  c'est-à-dire qu’une partie de la population est infectée par le virus.

* Le caractère `X` indique que la zone géographique est complètement infectée
  par le virus X.

* Le caractère `.` indique que la zone géographique est sans population (pas de
  population initiale ou population décédée).


Voici un exemple de grille de configuration:

~~~raw
H.XXHH.XHXH..H....H.XX....HX..X.XXX.XXH.
..X......H......X..H.H.X..HH.X.XH..X..HX
.....X.HXH....HX.XX.H..HHX.HH...HXX...XX
.XHXX..XXXXXQ......X...H.H...XX...X..X..
..HHH..X.....X...H...H...XX...X.X...X...
XHQX..XX..XX..X.....X..H.HX.....X......X
HXH......X...X..XHH...........X.....XX..
..XH.X...X......X...XX.HXH......HXH..H.H
XX....X...X.HX....X..XXXHX....X.XH...XHH
H.H...X..X.X...H.HXXH.....H..HXXXQ..X...
......HX.XH..X...HH.X.H.......XXX.X..X.H
HXX..XX....HHXH...H.X.H..XH..X.H......X.
.XHH....XXH..X..HH...XX...X..H...HHH.XX.
....QXXXX..X..X.....XX...HXH.....X..H...
..XXHHXX.XHHH.X.HQ........X..X...X...XX.
..XH.H.HX.HHXXXX..H..H.H.X.X....HXHH.H..
....X..H.X.X..X.X.X..X.H...X.H..HH....HH
..XH.H..X.XX...QX...H..H..X....X....H...
...X...H....H.XHH.X.....H..H.X.HX..H....
.X...XHXXQ...X..XX..HH..HHHXX.H...XH.XXX
~~~

## Règles d'évolution

À chaque jour, la carte de configuration est remplacée par une nouvelle carte
selon les règles suivantes :

1. **Vie et mort**: les gens naissent et meurent (soit à cause du virus, soit
   par cause naturelle).

2. **Propagation du virus**: soit le virus se propage à travers la population,
   soit il se résorbe.

3. **Contamination** après un jour, une population en quarantaines devient 
   complètement infectée


Les propagations de la population et du virus se font en fonction des
cellules voisines dans la carte de configuration. Pour chaque cellule `C` de
la carte, on considère ses 8 voisins numérotés de 1 à 8 tels que présentés
ci-dessous:

~~~raw
.....
.123.
.4C5.
.678.
.....
~~~

Les règles sont les suivantes:

* On dit qu’une population est infectée si elle est en quarantaine ou 
  complètement infectée

* Une zone sans population devient peuplée si elle possède **exactement trois**
  cellules voisines peuplées (populations saines, en quarantaine et infectées 
  confondues).  Si la majorité des trois cellules voisines est infectée alors 
  la zone devient infectée partiellement (c’est-à-dire en quarantaine), 
  sinon elle est considérée comme saine.

* Une zone avec population reste peuplée si elle possède **deux ou trois**
  voisins peuplés. De plus, une zone saine devient infectée partiellement 
  (c’est-à-dire en quarantaine), si la majorité de ses voisins est infectée.

* Une zone avec population en quarantaine devient complètement infectée après 
  une journée

* Une zone avec **moins de deux** ou **plus de trois** voisins vivants meurt
  (de solitude ou par étouffement).

## Comportement du programme

Votre programme devra avoir un comportement très précis afin d'automatiser la
correction. Vous devrez donc vous assurer de ne pas écrire de messages
superflus sur `stdout` et de bien écrire **tel quel** les messages d'erreurs
s'il y a lieu. En particulier, toute carte lue par votre programme doit passer
par l'**entrée standard** (`stdin`).

Tout d'abord, si aucun argument n'est spécifié par l'utilisateur, le programme
lit la carte de configuration depuis l'entrée standard, affiche `"Jour 0"`,
puis affiche la même carte, telle quelle. Ainsi, si vous entrez

~~~sh
$ bin/pandemic
H.XXHH.XHXH..H....H.XX....HX..X.XXX.XXH.
..X......H......X..H.H.X..HH.X.XH..X..HX
.....X.HXH....HX.XX.H..HHX.HH...HXX...XX
.XHXX..XXXXXQ......X...H.H...XX...X..X..
..HHH..X.....X...H...H...XX...X.X...X...
XHHX..XX..XX..X.....X..H.HX.....X......X
HXH......X...X..XHH...........X.....XX..
..XH.X...X......X...XX.HXH......HXH..H.H
XX....X...X.HX....X..XXXHX....X.XH...XHH
H.H...X..X.X...H.HXXH.....H..HXXXQ..X...
......HX.XH..X...HH.X.H.......XXX.X..X.H
HXX..XX....HHXH...H.X.H..XH..X.H......X.
.XHH....XXH..X..HH...XX...X..H...HHH.XX.
....QXXXX..X..X.....XX...HXH.....X..H...
..XXHHXX.XHHH.X.HQ........X..X...X...XX.
..XH.H.HX.HHXXXX..H..H.H.X.X....HXHH.H..
....X..H.X.X..X.X.X..X.H...X.H..HH....HH
..XH.H..X.XX...QX...H..H..X....X....H...
...X...H....H.XHH.X.....H..H.X.HX..H....
.X...XHXXQ...X..XX..HH..HHHXX.H...XH.XXX
~~~

alors le programme affichera

~~~sh
Jour 0
H.XXHH.XHXH..H....H.XX....HX..X.XXX.XXH.
..X......H......X..H.H.X..HH.X.XH..X..HX
.....X.HXH....HX.XX.H..HHX.HH...HXX...XX
.XHXX..XXXXXQ......X...H.H...XX...X..X..
..HHH..X.....X...H...H...XX...X.X...X...
XHHX..XX..XX..X.....X..H.HX.....X......X
HXH......X...X..XHH...........X.....XX..
..XH.X...X......X...XX.HXH......HXH..H.H
XX....X...X.HX....X..XXXHX....X.XH...XHH
H.H...X..X.X...H.HXXH.....H..HXXXQ..X...
......HX.XH..X...HH.X.H.......XXX.X..X.H
HXX..XX....HHXH...H.X.H..XH..X.H......X.
.XHH....XXH..X..HH...XX...X..H...HHH.XX.
....QXXXX..X..X.....XX...HXH.....X..H...
..XXHHXX.XHHH.X.HQ........X..X...X...XX.
..XH.H.HX.HHXXXX..H..H.H.X.X....HXHH.H..
....X..H.X.X..X.X.X..X.H...X.H..HH....HH
..XH.H..X.XX...QX...H..H..X....X....H...
...X...H....H.XHH.X.....H..H.X.HX..H....
.X...XHXXQ...X..XX..HH..HHHXX.H...XH.XXX
~~~

L'utilisateur peut spécifier **un et un seul** argument représentant le nombre
de jours à simuler.

Par exemple, on peut visualiser une simulation de deux jours en
saisissant

~~~sh
$ bin/pandemic 2
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
..................XHX...................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
~~~

alors le résultat est

~~~sh
Jour 0
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
..................XHX...................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
Jour 1
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
...................Q....................
...................Q....................
...................Q....................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
Jour 2
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
..................QXQ...................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
........................................
~~~

**Remarque:** le jour 0 est toujours affiché et correspond à la configuration
initiale.

## Gestion des erreurs

Votre programme devra gérer certains cas d'erreurs en affichant **exactement**
les messages d'erreurs demandés (sinon les tests automatisés vont échouer):

1. Si l'utilisateur fournit plus d'un argument au programme, alors le message
   d'erreur devrait être:
   ```sh
   Erreur: Attendu un seul argument: le nombre de jours à simuler.
   ```

2. Si l'utilisateur fournit un argument invalide, alors le message d'erreur
   devrait être:
   ```sh
   Erreur: Le nombre de jours à simuler doit être entre 0 et 100.
   ```

3. Si l'utilisateur fournit une cellule invalide, alors on doit plutôt écrire
   ```sh
   Erreur: Caractère `C` inattendu, attendu `H`, `Q`, `X` ou `.`.
   ```
   où `C` est à remplacer par le premier caractère erroné saisi par
   l'utilisateur.

   Cette erreur est aussi produite si l'utilisateur essaye de saisir une carte
   trop courte:
   ```sh
   Erreur: Caractère `EOF` inattendu, attendu `H`, `Q`, `X` ou `.`.
   ```

   Dans le cas d'une carte trop longue, seules les cases nécessaires sont lues,
   le reste est ignoré.

## Marche à suivre

Afin de compléter ce travail pratique, vous devrez suivre les étapes suivantes
(pas nécessairement dans l'ordre):

1. Clonez (mais sans faire de *fork*) le [dépôt du
   projet](https://gitlab.com/kadouche/inf3135-Hiver2018-tp1-Projet).
2. Créez un dépôt nommé `inf3135-hiver2018-tp1`.
3. Implémentez le programme `pandemi.c` en suivant le standard C11.
4. Complétez le `Makefile` de l'application pour qu'il compile votre programme
   `pandemi.c`.
5. Complétez le fichier `README.md` en respectant le format Markdown.
6. Versionnez fréquemment l'évolution de votre projet avec Git.

### Clone et création du dépôt

Vous devez cloner le dépôt fourni et l'héberger sur la plateforme
[Gitlab](https://gitlab.com/). Ne faites pas de *fork*, car cela pourra
entraîner certains problèmes. Votre
dépôt devra se nommer **exactement** `inf3135-hiver2018-tp1` et l'URL devra être
**exactement** `https://gitlab.com/<utilisateur>/inf3135-hiver2018-tp1`, où
`<utilisateur>` doit être remplacé par votre identifiant GitLab. Il devra être
**privé** et accessible seulement par vous et par `kadouche` et les deux 
correcteurs  `rmTheZ` et `sim590`.


Votre programme doit être écrit dans un seul fichier nommé `pandemi.c`, placé
dans le répertoire `src`.  Le programme doit être écrit en C, compatible avec
C11 et compilable avec `gcc` version 4.8 ou ultérieure. Assurez-vous qu'il
fonctionne correctement sur les serveurs Malt ou Java.

### Makefile

Vous devez ajouter une règle dans le fichier Makefile pour compiler votre
programme. N'oubliez pas les dépendances s'il y en a!

La règle devrait avoir comme cible `bin/pandemic` (l'exécutable), qui doit être
produit en compilant le fichier `src/pandemi.c`.

### Fichier `README.md`

Vous devez modifier le fichier `README.md` pour ajouter différentes
informations. Assurez-vous de répondre minalement aux questions suivantes, en
plus de celles qui y sont mentionnées:

* À quoi sert votre programme?
* Comment le compiler?
* Comment l'exécuter?
* Quels sont les formats d'entrées et de sorties?
* Quels sont les cas d'erreurs gérés?

Vous devez utiliser le format Markdown pour écrire une documentation claire et
lisible. Soignez la qualité de votre français, qui sera évalué plus
particulièrement dans le fichier `README.md`.

### Git

Les sources de votre programme devront être versionnées à l'aide de Git.  Vous
devez cloner le gabarit du projet fourni et rajouter vos modifications à l'aide
de *commits*.  N'oubliez pas de modifier le fichier `.gitignore` en fonction de
votre environnement de développement. Aussi, assurez-vous de ne pas versionner
de fichiers inutiles!

Adresse du dépôt à clôner:
[https://gitlab.com/kadouche/inf3135-Hiver2018-tp1-Projet]
(https://gitlab.com/kadouche/inf3135-Hiver2018-tp1-Projet).

## Correction

L'exécution de votre programme sera vérifiée automatiquement grâce au script
`tests.sh` fourni avec le gabarit du projet. Notez que vous n'avez pas à
connaître la syntaxe d'un script shell pour pouvoir l'utiliser.

Pour faciliter votre développement, vous avez accès à cinq des tests utilisés
pour corriger vos travaux (répertoire `tests/`) ainsi qu'au script de tests
(`tests.sh`). Il suffit d'entrer `make check` pour lancer la suite de tests.

Vous avez aussi à votre disposition le programme `generator` permettant de
générer des cartes de configuration aléatoirement.  Vous pouvez vous en servir
pour écrire vos propres tests.

Lisez le fichier `README.md` présent dans le gabarit du projet pour comprendre
comment utiliser le générateur de cartes ou le script de tests.

### Barème

Les critères d'évaluation sont les suivants:

| Critère            | Points |
| -------            | -----: |
| Fonctionnabilité   | /40    |
| Qualité du code    | /20    |
| Documentation      | /15    |
| Makefile           | /10    |
| Utilisation de Git | /15    |
| Total              | /100   |

Les critères plus détaillés sont décrits ci-bas:

* **Fonctionnabilité (40%)**: 10 tests, 4 points par test. Le programme passe
  les tests en affichant le résultat attendu.

* **Qualité du code (20%)**: Les identifiants utilisés sont significatifs et
  uniformes, le code est bien indenté, il y a de l'aération autour des
  opérateurs et des parenthèses, le programme est simple et lisible. Pas de
  code commenté ou de commentaires inutiles. Il est décomposé en petites
  fonctions qui effectuent des tâches spécifiques. La présentation est soignée
  de façon générale.

* **Documentation (15%)**: Le fichier `README.md` est complet et respecte le
  format Markdown. L'en-tête du fichier `pandemi.c` est bien documentée, de
  même que chacune des fonctions (*docstrings*) en suivant le standard Javadoc.

* **Makefile (10%)**: Le Makefile permet de compiler le programme avec la
  commande `make bin/pandemic` et de lancer les tests automatiquement avec
  `make check`.

* **Utilisation de Git (15%)**: Les modifications sont réparties en plusieurs
  *commits*. Le fichier `.gitignore` a été mis à jour. Les messages de *commit*
  sont significatifs et respectent le format suggéré (première ligne courte,
  suivi de paragraphes si nécessaires).

### Pénalités

Tout programme ne passant pas au moins le test 0 (compilation via Makefile) se
verra automatiquement attribuer **la note 0**.  Vous devez vous assurer que
votre programme passe au moins ce test grâce au script `tests.sh` fourni.

En outre, si vous ne respectez pas les critères suivants, une pénalité de
**50%** sera imposée :

- Votre dépôt doit se nommer **exactement** `inf3135-hiver2018-tp1` sur GitLab;

- L'URL de votre dépôt doit être **exactement**
  `https://gitlab.com/<utilisateur>/inf3135-hiver2018-tp1` où `<utilisateur>`
  doit être remplacé par votre identifiant GitLab.

- Votre dépôt doit être **privé**.

- Les utilisateurs `kadouche`, `rmTheZ` et `sim590` doivent avoir
  accès à votre projet en mode *Developer*.

## Remise


La remise doit se faire sur **Moodle** et sur gitlab en ajoutant les utilisateurs 
`kadouche`, `rmTheZ` et `sim590`en mode *Developer*.

Le travail doit être remis au plus tard le **18 février à 23h59**. À partir de
minuit, une pénalité de **2%** par heure de retard sera appliquée.
